# cse 234 - Homework 4 #
https://bitbucket.org/kfserran/hw4/src/master/

### Given n, we shall determine: ###

* If n is prime.
* If n is a Safe Prime.
* If n is a Safe Prime, what the value of the Sophie Germain Prime used to create n.
* If n is a Safe Prime, which g \in {1 ... 20} are full period generators mod n.
