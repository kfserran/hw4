from math import sqrt

def is_prime(num):
  if num == 2:
    return True
  if (num < 2) or (num % 2 == 0):
    return False

  # return all(num % i for i in range(3, int(sqrt(num)) + 1, 2))
  
  for i in range(3, int(sqrt(num)) + 1, 2):
    if num % i == 0:
      return False

  return True


# def is_prime(num)
#     # ...
#     return not any(num % i == 0 for i in range(3, int(sqrt(num)) + 1, 2))


def is_safe_prime(p):
  if is_prime(p):
    q = (p - 1) / 2
    if is_prime(q):
      return (True, q)
    else:
      return (False, -1)  
  else:
    return (False, -2)  
  
def get_generators_list(z, m):
  results = []
  list_up_to_m = [num for num in range(m)]
  for g in range(2, z):
    temp = list_up_to_m
    for i in range(n):
      temp.remove(pow(g,i,n))
      if len(temp) == 0:
        results.append(g)
        break
  return results

if __name__ == "__main__":
  n = 2649376219191757686333291073027588009793925231566294290337286424331787812414080960960191670815548639

  # print(is_prime(n))
  n_is_safe_prime, sophie_germain_prime = is_safe_prime(n)
  if n_is_safe_prime:
    print("n is a Safe Prime created by the Sophie Germain prime: \n{}\n".format(int(sophie_germain_prime)))
    print("The full period generators are: \n{}\n".format(get_generators_list(20,n)))
  else:
    print("Can not determine if n is a safe prime.")

